﻿/**
 * Win + F1~F3 to switch input method
 */
; Language Identifier: https://learn.microsoft.com/en-us/previous-versions/ms957130(v=msdn.10)
#F1::SetInputLang(0x0804) ; Chinese (PRC)
#F2::SetInputLang(0x0411) ; Japanese
#F3::SetInputLang(0x0409) ; English (USA)

; Ref: https://www.autohotkey.com/boards/viewtopic.php?p=233011#p233011
SetInputLang(Lang)
{
    WinExist("A")
    ControlGetFocus, CtrlInFocus
    PostMessage, 0x50, 0, % Lang, %CtrlInFocus%
}

/**
 * Shift キーで英数切り替え
 * MS IME と Google 日本語入力での動作は確認している
 */
~Shift Up::
    layout := GetKeyboardLayout()
    if (layout == "68224017") {
        Send, {vkF0sc03A}
    }
return
; Ref: https://superuser.com/a/579449/485163
~Shift & F13:: return

/**
 * Shift + CapsLock in Japanese IME to toggle CapsLock
 */
+sc03A:: ; sc03A is a special version of CapsLock. Can be distinguished from CapsLock without shift modifier
    layout := GetKeyboardLayout()
    if (layout == "68224017") {
        Send, {CapsLock}
    }
return

/**
 * Shift + Space to write a full width space in Chinese input method
 */
$+Space::
    layout := GetKeyboardLayout()
    If (layout == 0x8040804) {
        Send, {U+3000}
    } Else {
        Send, +{Space}
    }
return

; Ref: https://github.com/jNizM/AHK_DllCall_WinAPI/blob/master/src/Keyboard%20Input%20Functions/GetKeyboardLayout.ahk
GetKeyboardLayout() {
    return DllCall("GetKeyboardLayout", "UInt", DllCall("GetWindowThreadProcessId", "UInt", WinActive("A"), "UInt", 0), "UInt")
}
