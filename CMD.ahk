/**
 * ConEmu か Cmder を使っていることが前提
 * 
 * Win + C で適当なパスで CMD を立ち上げる
 * また Win + B で Bash を立ち上げる
 */


#c:: RunConEmuTask("cmd")
#b:: RunConEmuTask("bash")

RunConEmuTask(Task) {
    ConEmuDir := "..\Cmder\vendor\conemu-maximus5\"
    Location := ActiveFolderPath()
    if (Location == "") {
        Location := "%USERPROFILE%"
    }
    Run, % ConEmuDir . "ConEmu64.exe -single -dir """ . Location . """-run {" . Task . "}"
}

; Ref: http://autohotkey.com/board/topic/70960-detect-current-windows-explorer-location/?p=495673
;------------- Explorer Path Functions  ------------

; use #IfWinActive ahk_group ExplorerGroup
; to make sure the active window is an
; Explorer window before calling this function
;
ActiveFolderPath()
{
    return PathCreateFromURL( ExplorerPath(WinExist("A")) )
}

; slightly modified version of function by jethrow
; on AHK forums.
;
ExplorerPath(_hwnd)
{
    for Item in ComObjCreate("Shell.Application").Windows
        if (Item.hwnd = _hwnd)
            return, Item.LocationURL
}

; function by SKAN on AHK forums
;
PathCreateFromURL( URL )
{
    VarSetCapacity( fPath, Sz := 2084, 0 )
    DllCall( "shlwapi\PathCreateFromUrl" ( A_IsUnicode ? "W" : "A" ), Str,URL, Str,fPath, UIntP,Sz, UInt,0 )
    return fPath
}