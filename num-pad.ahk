/**
 * ScrollLock と NumLock がないノートパソコンで
 * Fn + C でテンキー切り替え
 * 
 * マッピングは
 * 7890        7890 
 * uiop   →   456=
 * jkl;        123.
 * SPACE       0

 * qwer        ()-+
 * asdf        %^/*
 *   cv        <comb(, )><perm(, )>
 *
 * 注：US 配列を想定
 */
#If (GetKeyState("ScrollLock", "T")) ; 電卓ソフト以外でも Fn + c でオンにできる
    Space:: Send, {Numpad0}
    j::     Send, {Numpad1}
    k::     Send, {Numpad2}
    l::     Send, {Numpad3}
    u::     Send, {Numpad4}
    i::     Send, {Numpad5}
    o::     Send, {Numpad6}
    7::     Send, {Numpad7}
    8::     Send, {Numpad8}
    9::     Send, {Numpad9}
    SC027:: Send, {.}
    p::     Send, {=}
    q::     Send, {(}
    w::     Send, {)}
    e::     Send, {-}
    r::     Send, {+}
    a::     Send, {`%}
    s::     Send, {^}
    d::     Send, {/}
    f::     Send, {*}
    +c::    Send, comb(, ){Left}{Left}{Left}
    +v::    Send, perm(, ){Left}{Left}{Left}
;    =::     Send, {+}
;    [::     Send, {/}
;    ]::     Send, {*}
;    \::     Send, {^}
#If
