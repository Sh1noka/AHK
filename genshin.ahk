﻿#MaxHotkeysPerInterval 200
#MaxThreadsPerHotkey 2
#UseHook
Enabled := True
SpamRate := 50

SetCapsLockState AlwaysOn

#CapsLock::
    Enabled := !Enabled
    If (Enabled) {
        SetCapsLockState AlwaysOn
    }
    Else {
        SetCapsLockState AlwaysOff
    }
return

#IfWinActive ahk_exe GenshinImpact.exe
{
    ~LButton::
        If (!Enabled) {
            return
        }

        While (GetKeyState("LButton", "P")) {
            If (GetKeyState("CapsLock", "P")) {
                Click Down
            } Else {
                Click
            }
            Sleep SpamRate
        }
    return

    ~e::
        If (!Enabled) {
            return
        }

        While (GetKeyState("e", "P")) {
            If (!GetKeyState("CapsLock", "P")) {
                Send e
                Sleep SpamRate
            }
        }
    return

    ~q::
        If (!Enabled) {
            return
        }

        While (GetKeyState("q", "P")) {
            If (!GetKeyState("CapsLock", "P")) {
                Send q
                Sleep SpamRate
            }
        }
    return

    ~f::
        If (!Enabled) {
            return
        }

        While (GetKeyState("f", "P")) {
            Send f
            Sleep SpamRate
        }
    return

    ~1::
        If (!Enabled) {
            return
        }

        While (GetKeyState("1", "P")) {
            Send 1
            Sleep SpamRate
        }
    return

    ~2::
        If (!Enabled) {
            return
        }

        While (GetKeyState("2", "P")) {
            Send 2
            Sleep SpamRate
        }
    return

    ~3::
        If (!Enabled) {
            return
        }

        While (GetKeyState("3", "P")) {
            Send 3
            Sleep SpamRate
        }
    return

    ~4::
        If (!Enabled) {
            return
        }

        While (GetKeyState("4", "P")) {
            Send 4
            Sleep SpamRate
        }
    return
}